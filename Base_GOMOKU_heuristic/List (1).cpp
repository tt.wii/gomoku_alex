#include "stdafx.h"
#include "Base_GOMOKU_heuristic.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int		In_Game::getValue()
{
	return (this->value);
}

void	Node::setValue(int &val)
{
	this->value = val;
}

void	In_Game::minMax() //ia = max joueur = min
{
	int min = 0;
	int max = 0;
	std::vector<int> vec;
	for (std::vector<Node>::iterator it = this->list.begin(); it != this->list.end(); ++it) //ajoute le coup a la map et ajoute le node au vector
	{
		for (std::vector<Node>::iterator at = it->next.begin(); at != it->next.end(); ++at)
		{
			if (at == it->next.begin()) //first time
				min = at->getValue();
			else if (at->getValue() < min)
				min = at->getValue();
		}
		it->setValue(min);
		vec.push_back(min);
	}
	value = *std::max_element(vec.begin(), vec.end());
}

void	In_Game::play(std::vector<std::string> &game) //fonction prevoir coup
{
	std::regex a("[1]{1,}");
	std::regex b("[2]{1,}");
	int x = 0;
	int y = 0;
	int max = 0;

	IA_Player *ia = new IA_Player(a, b);
	for (int i = 0; i != 20; i++) //ajoute le coup a la map et ajoute le node au vector
		for (int j = 0; j != 20; j++)
		{
			Node node(game);
			if (node.map[i][j] == '0')
			{
				node.map[i][j] = '1';
				int val = ia->heuristic_evaluation(node.map);
				this->list.push_back(node);
				if (i == 0 && j == 0)
					max = val;
				if (val > max)
				{
					x = i;
					y = j;
					max = val;
				}
				for (int i = 0; i != REAL_MAP_LENGTH; i++)
				{
					//std::cout << "zizi" << std::endl;
					std::cout << game[i] << std::endl;
				}
			}
		}
	game[y][x] = '1';
	std::cout << y << "," << x << std::endl;
	//std::cout << "y = " << y << " x = " << x << std::endl;
	//std::cout << "Il sort de play" << std::endl;
}

void	In_Game::displayAllNode()
{
	for (std::vector<Node>::iterator it = this->list.begin(); it != this->list.end(); ++it) //affiche toute les map
		for (std::vector<Node>::iterator ot = it->next.begin(); ot != it->next.end(); ++ot)
		{
			for (std::vector<std::string>::iterator at = ot->map.begin(); at != ot->map.end(); ++at)
				std::cout << *at << std::endl;
		}
}