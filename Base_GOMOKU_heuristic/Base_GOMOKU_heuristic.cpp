#include "stdafx.h"
#include "Base_GOMOKU_heuristic.h"

Board::Board() // initialiser le vector au lieu du tableau de int a 2D
{
	for (int i = 0; i != REAL_MAP_LENGTH; i++)
	{
		this->map.push_back("00000000000000000000");
	}
}

void Board::Actualize_Map(int height, int length, char white_black)
{
	//std::cout << "length = " << length << " height = " << height << std::endl;
	this->map[length][height] = white_black;
	//this->Display_map();
}

void Board::Display_map()
{
	for (int i = 0; i != REAL_MAP_LENGTH; i++)
	{
		std::cout << map[i] << std::endl;
	}
}

// ------------------------------------------------------------IN GAME-----------------------------------

void In_Game::SetWords_Of_HUMAN(std::string new_words_of_HUMAN)
{
	this->Words_Of_HUMAN = new_words_of_HUMAN;
}

void In_Game::New_Positions(std::string turn)
{
	std::string first;
	std::string second;
	int virgule = 0;

	for (int i = 0; i != turn.length(); ++i)
	{
		if (turn[i] == ',')
			virgule++;
		if (isdigit(turn[i]))
		{
			if (virgule == 0)
				first = first + turn[i];
			else if (virgule == 1)
				second = second + turn[i];
		}
	}
	this->SetFirst_Number(std::stoi(first)); // inversion des 2 par rapport a la map du jeu
	this->SetSecond_Number(std::stoi(second));
}

void In_Game::game_initialisation() // dans cette fonction on doit savoir qui commence
{
	// 1- recevoir le start
	// 2- une fois recu, tout initialiser et attendre le BEGIN(on commence), ou le TURN(l'autre commence)

	remove("C:/Users/Marsouin/source/repos/Base_GOMOKU_heuristic/Release/OUTPUT_IA.txt");
	std::string get_input;
	//std::getline(std::cin, get_input);
	//std::size_t found = get_input.find("START");
	//int first_iteration = 0;

	while (true)
	{
		std::getline(std::cin, get_input);
		this->my_file.open("C:/Users/Marsouin/source/repos/Base_GOMOKU_heuristic/Release/OUTPUT_IA.txt", std::ios_base::app);
		my_file << get_input;
		my_file << "\n";
		my_file.close();
		if ((get_input.compare(0, 5, "START")) == 0)
		{
			std::cout << "OK" << std::endl;
		}
		else if (get_input == "BEGIN")
		{
			this->i = 1;
			this->which_turn = 1;
			if ((this->loop_game(get_input)) == 84)
				return;
		}
		else if ((get_input.compare(0, 4, "TURN")) == 0)
		{
			//std::cout << "TURN DETECTE" << std::endl;
			this->i = 1;
			this->which_turn = 0;
			if ((this->loop_game(get_input)) == 84)
				return;
		}
	}
}

int In_Game::loop_game(std::string get_input)
{
	int compt = 0;
	Board *board = new Board();
	while (42) // faire modulo pour savoir quel tour c'est (on part du principe qu'on commence pas) -> le premier joueur recoit begin
	{
		//std::cout << "tour de (0.2=humain) " << GetWhich_turn() << std::endl;
		if (compt > 0 && this->GetWhich_turn() % 2 == 0)
		{
			std::getline(std::cin, get_input);
			//std::cout << "new input = " << get_input << std::endl;
		}
		if (this->GetWhich_turn() % 2 == 0) // on considere que c'est le tour de l'adversaire qui a commence a jouer
		{
			//int restart_or_not = 10;
			//restart_or_not = this->Game_Info_File();
			this->last_input_received = get_input;
			//std::cout << "ADVERSAIRE" << std::endl;
			this->SetWords_Of_HUMAN(this->GetLast_Input_Received());
			this->New_Positions(this->GetLast_Input_Received());
			board->Actualize_Map(this->GetFirst_Number(), this->GetSecond_Number(), '2'); // recuperer dans "TURN 2,3"
			//board->Display_map();
			this->which_turn++;
		}
		else if (this->GetWhich_turn() % 2 == 1) // c'est le tour de l'IA 1 % 2
		{
			
		}
		compt++;
	}
	delete board; // ATTENTION IL NE RENTRE PEUT ETRE PAS ICI
	this->my_file.close();
	return 0;
}

int	main(int argc, char **argv)
{
	In_Game *main_loop = new In_Game();

	main_loop->game_initialisation();
	//main_loop->loop_game();
	delete main_loop;
	return (0);
}