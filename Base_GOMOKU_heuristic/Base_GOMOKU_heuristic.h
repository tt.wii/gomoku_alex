#ifndef _BASE_GOMOKU_HEURISTIC_H_
# define _BASE_GOMOKU_HEURISTIC_H_

#include "stdafx.h"
#include <iostream>
#include <regex>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>

#define REAL_MAP_LENGTH 20
#define MAP_LENGTH 19

/*class IA_Player // classe qui fera tous les calculs pour savoir ou mettre le pion
{
private:
std::string Words_Of_IA; // commandes envoyees par l'IA
public:
IA_Player() {};
~IA_Player() {};
void IA_moove(); // fonction qui fera tous les calculs
std::string GetWords_Of_IA() { return this->Words_Of_IA; }
void SetWords_Of_IA(std::string new_words) { this->Words_Of_IA = new_words; }
};*/

/*class List
{
	int value;
	std::vector<Node>	list;

public:
	List();
	~List() {}

	void	play(std::vector<std::string> &);
	void	displayAllNode();
	void	minMax();
	int		getValue();
};*/

class Node //Coup
{
	int value;

public:
	std::vector<std::string>	map;
	std::vector<Node>			next; //vector

	Node(std::vector<std::string> arg) : map(arg) { }
	~Node() {}

	void	setValue(int &);
	int		getValue() { return (this->value); }
};

class IA_Player
{
private:
	const std::regex white_pieces;
	const std::regex black_pieces;
	std::smatch matched;
	int white_heuristic; // valeur des noirs
	int black_heuristic; // valeur des blancs
	int heuristic_value; // valeur d'evaluation finale
public:
	IA_Player(std::regex white, std::regex black) : white_pieces(white), black_pieces(black) {};
	~IA_Player() {};
	int countMatchInRegex(std::string s, std::string re); // compte le nombre de match de la regex
	int heuristic_evaluation(std::vector<std::string> &map);
	void pars_columns(std::vector<std::string> &map);
	void pars_lines(std::vector<std::string> &map);
	void pars_lines_row(std::vector<std::string> &map);
	void pars_first_diag(std::vector<std::string> &map);
	void pars_second_diag(std::vector<std::string> &map);
	void columns_row(std::string &columns);
	void malus(std::vector<std::string> &map);
	int apply_regex_white(std::string str, std::regex the_regex, int bonus_value);
	int apply_regex_black(std::string str, std::regex the_regex, int bonus_value);
	void get_first_diag_line(int iterations, int i, std::string &diag, std::vector<std::string> map, int y);
	void get_second_diag_line(int iterations, int i, std::string &diag, std::vector<std::string> map, int y);
	void heuristic_final() { this->heuristic_value = this->black_heuristic - white_heuristic; } // consideration que nous sommes les noirs;
};

class In_Game // tete du programme (main_class + In_Game)
{
private:
	std::ofstream my_file;
	int which_turn; // % pour savoir c'est a qui de jouer x % 2 == 0 || x % 2 == 1
	int i;
	std::string last_input_received;
	int first_number;
	int second_number;
	std::string Words_Of_HUMAN; // commandes envoyees par le jeu

	int value;
	std::vector<Node>	list;
public:
	In_Game() : which_turn(which_turn), last_input_received(last_input_received), first_number(first_number),
		second_number(second_number), Words_Of_HUMAN(Words_Of_HUMAN) {};
	~In_Game() {}
	int Game_Info_File();
	void New_Positions(std::string turn);
	int loop_game(std::string get_input);
	void Words_Of_IA(int first_nb, int second_nb); // dans cette fonction, l'IA va envoyer les commandes au jeu
	void In_Game::game_initialisation(); // fonction qui recoit tout avant la boucle de gameplay (START ou BEGIN)

	int GetWhich_turn() { return this->which_turn; }
	void SetWords_Of_HUMAN(std::string new_words_of_HUMAN); // set la derniere phrase envoyee par le jeu
	std::string GetLast_Input_Received() { return this->last_input_received; }
	void SetFirst_Number(int number) { this->first_number = number; }
	void SetSecond_Number(int number) { this->second_number = number; }
	int GetFirst_Number() { return this->first_number; }
	int GetSecond_Number() { return this->second_number; }

	int block_columns(std::vector<std::string> &map);
	int block_lines(std::vector<std::string> &map);
	int attack_columns(std::vector<std::string> &map);
	int attack_lines(std::vector<std::string> &map);


	void attack_or_defense(std::vector<std::string> &map);
	void attack();
	void defense();

	void	play(std::vector<std::string> &);
	void	displayAllNode();
	void	minMax();
	int		getValue();
};

class Board
{
private:
	//std::vector< std::vector<int> > vec_board;
	std::vector<std::string> map;
	int dimensions;
public:
	Board();
	~Board() {}
	void Actualize_Map(int height, int length, char white_black); // il s'agit d'un seter de board
	std::vector< std::string > &GetBoard() { return this->map; }
	void Display_map();
};

#endif /* _BASE_GOMOKU_HEURISTIC_H_ */