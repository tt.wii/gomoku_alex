#include "stdafx.h"
#include "Base_GOMOKU_heuristic.h"

int IA_Player::apply_regex_white(std::string str, std::regex the_regex, int bonus_value)
{
	if (std::regex_search(str, this->matched, the_regex))
	{
		this->white_heuristic = this->white_heuristic + bonus_value;
		return (1);
	}
	return (0);
}

int IA_Player::countMatchInRegex(std::string s, std::string re)
{
	std::regex words_regex(re);
	auto words_begin = std::sregex_iterator(s.begin(), s.end(), words_regex);
	auto words_end = std::sregex_iterator();
	return std::distance(words_begin, words_end);
}

int IA_Player::apply_regex_black(std::string str, std::regex the_regex, int bonus_value)
{
	if (std::regex_search(str, this->matched, the_regex))
	{
		this->black_heuristic = this->black_heuristic + bonus_value;
		return (1);
	}
	return (0);
}

void IA_Player::columns_row(std::string &columns) // dans cette fonction, il faut modifier black/white heuristique en fonction des suites
{ // ici ajouter que les bonus
	std::regex row_white("[1]{4,}");
	std::regex row_black("[2]{4,}");

	if (columns.length() <= 1)
		return;
	if ((apply_regex_white(columns, row_white, 13)) == 1)
		return;
	row_white = "[1]{3,}";
	if ((apply_regex_white(columns, row_white, 6)) == 1)
		return;
	row_white = "[1]{2,}";
	if ((apply_regex_white(columns, row_white, 4)) == 1)
		return;
	if ((apply_regex_black(columns, row_black, 13)) == 1)
		return;
	row_black = "[2]{3,}";
	if ((apply_regex_black(columns, row_black, 6)) == 1)
		return;
	row_black = "[2]{2,}";
	if ((apply_regex_black(columns, row_black, 4)) == 1)
		return;
}

void IA_Player::pars_columns(std::vector<std::string> &map)
{
	std::string column("");

	for (int y = 0; y != MAP_LENGTH; y++)
	{
		for (int i = 0; i != MAP_LENGTH; i++)
		{
			column = column + map[i][y];
		}
		this->columns_row(column);
		column = "";
	}
}

void IA_Player::pars_lines(std::vector<std::string> &map)
{
	for (int i = 0; i != REAL_MAP_LENGTH; i++) // parcourage lignes
	{
		if (std::regex_search(map[i], this->matched, this->white_pieces)) // faire fonction qui parse diagonales + colonnes + 
		{
			this->white_heuristic = this->white_heuristic + 8 * this->countMatchInRegex(map[i], "[1]") /*this->matched.length()*/;
		}
		if (std::regex_search(map[i], this->matched, this->black_pieces)) // faire fonction qui parse diagonales + colonnes + 
		{
			this->black_heuristic = this->black_heuristic + 8 * this->countMatchInRegex(map[i], "[2]") /*this->matched.length()*/;
		}
	}
}

void IA_Player::pars_lines_row(std::vector<std::string> &map)
{
	int white_value = 0;
	int black_value = 0;

	for (int i = 0; i != MAP_LENGTH; i++)
	{
		std::regex row_white("[1]{4,}");
		if ((apply_regex_white(map[i], row_white, 13)) == 1)
			white_value++;
		row_white = "[1]{3,}";
		if ((white_value == 0 && apply_regex_white(map[i], row_white, 6)) == 1)
			white_value++;
		row_white = "[1]{2,}";
		if ((white_value == 0 && apply_regex_white(map[i], row_white, 4)) == 1)
			white_value++;

		std::regex row_black("[2]{4,}");
		if ((apply_regex_black(map[i], row_black, 13)) == 1)
			black_value++;
		row_black = "[2]{3,}";
		if ((black_value == 0 && apply_regex_black(map[i], row_black, 6)) == 1)
			black_value++;
		row_black = "[2]{2,}";
		if ((black_value == 0 && apply_regex_black(map[i], row_black, 4)) == 1)
			black_value++;
		white_value = 0;
		black_value = 0;
	}
}

void IA_Player::get_first_diag_line(int iterations, int i, std::string &diag, std::vector<std::string> map, int y)
{
	while (y != iterations + 1)
	{
		diag = diag + map[i][y];
		i++;
		y++;
	}
	this->columns_row(diag);
	diag = "";
}

void IA_Player::pars_first_diag(std::vector<std::string> &map) // bas gauche jusqu'a haut droite
{
	std::string diag;
	int i = MAP_LENGTH;
	int y = 0;
	int number = 0;

	while (true)
	{
		if (number != MAP_LENGTH && i != 0)
		{
			get_first_diag_line(number, i, diag, map, y);
			number++;
			i--;
		}
		else // milieu de la map, changement diagonale
		{
			i = 0;
			get_first_diag_line(number, i, diag, map, y);
			y++;
		}
		if (i == 0 && y == REAL_MAP_LENGTH)
			break;
	}
}

void IA_Player::get_second_diag_line(int iterations, int i, std::string &diag, std::vector<std::string> map, int y)
{
	//while (y != iterations + 1)
	while (y != (MAP_LENGTH - iterations - 1))
	{
		diag = diag + map[i][y];
		i++;
		y--;
	}
	this->columns_row(diag);
	diag = "";
}

void IA_Player::pars_second_diag(std::vector<std::string> &map) // bas droite jusqu'a haut gauche (il faut tout inverser normalement)
{
	std::string diag;
	int i = MAP_LENGTH;
	int y = MAP_LENGTH;
	int number = 0;

	while (true)
	{
		if (i != 0)
		{
			get_second_diag_line(number, i, diag, map, y);
			number++;
			i--;
		}
		else // milieu de la map, changement diagonale
		{
			i = 0;
			get_second_diag_line(number, i, diag, map, y);
			y--;
		}
		if (i == 0 && y == -2)
			break;
	}
}

void IA_Player::malus(std::vector<std::string> &map) // malus
{ // si y a des palets qui sont sur des bords (-2)
	for (int i = 0; i != REAL_MAP_LENGTH; i++)
	{
		for (int y = 0; y != REAL_MAP_LENGTH; y++)
		{
			if (y == 0 || y == MAP_LENGTH)
			{
				if (map[i][y] == '1')
					this->white_heuristic = this->white_heuristic - 2;
				else if (map[i][y] == '2')
					this->black_heuristic = this->black_heuristic - 2;
			}
			if (i == 0 || i == MAP_LENGTH)
			{
				if (map[i][y] == '1')
					this->white_heuristic = this->white_heuristic - 2;
				else if (map[i][y] == '2')
					this->black_heuristic = this->black_heuristic - 2;
			}
		}
	}
}

int IA_Player::heuristic_evaluation(std::vector<std::string> &map) // prends une map en parametre et retourne un nombre selon si on est dans la merde ou non
{
	this->black_heuristic = 0;
	this->white_heuristic = 0;
	pars_lines(map); // le tout premier
	pars_columns(map); // row columns
	pars_lines_row(map); // row lines
	pars_first_diag(map); // row diag
	pars_second_diag(map);
	malus(map);
	this->heuristic_final();
	return (this->heuristic_value);
}